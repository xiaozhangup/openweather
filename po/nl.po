# Dutch translation for gnome-shell-extension-openweather
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Peter van Ieperen <p.v.ieperen77@gmail.com>, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-09 14:39-0300\n"
"PO-Revision-Date: 2016-03-06 23:50+0100\n"
"Last-Translator: Christian METZLER <neroth@xeked.com>\n"
"Language-Team: \n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.8.7\n"

#: src/extension.js:171
msgid "..."
msgstr "..."

#: src/extension.js:323
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org werkt niet zonder een api-key.\n"
"Stel de standaard api-key in in de voorkeuren of registreer op http://"
"openweathermap.org/appid en voeg de persoonlijke api-key toe in de "
"voorkeuren."

#: src/extension.js:434 src/extension.js:446
#, javascript-format
msgid "Can not connect to %s"
msgstr "Kon niet verbinden met %s"

#: src/extension.js:737 src/weather-settings.ui:302
msgid "Locations"
msgstr "Locaties"

#: src/extension.js:741
msgid "Reload Weather Information"
msgstr "Herlaad Weer Informatie"

#: src/extension.js:745
#, fuzzy
msgid "Weather data by:"
msgstr "Weer data voorzien door: "

#: src/extension.js:748
msgid "Weather Settings"
msgstr "Weer Instellingen"

#: src/extension.js:771
#, javascript-format
msgid "Can not open %s"
msgstr "Kon %s niet openen"

#: src/extension.js:819 src/prefs.js:1072
msgid "Invalid city"
msgstr "Ongeldige stad"

#: src/extension.js:830
msgid "Invalid location! Please try to recreate it."
msgstr "Ongeldige locatie! Probeer a.u.b. om deze opnieuw toe te voegen."

#: src/extension.js:870 src/weather-settings.ui:574
msgid "°F"
msgstr ""

#: src/extension.js:872 src/weather-settings.ui:575
msgid "K"
msgstr ""

#: src/extension.js:874 src/weather-settings.ui:576
msgid "°Ra"
msgstr ""

#: src/extension.js:876 src/weather-settings.ui:577
msgid "°Ré"
msgstr ""

#: src/extension.js:878 src/weather-settings.ui:578
msgid "°Rø"
msgstr ""

#: src/extension.js:880 src/weather-settings.ui:579
msgid "°De"
msgstr ""

#: src/extension.js:882 src/weather-settings.ui:580
msgid "°N"
msgstr ""

#: src/extension.js:884 src/weather-settings.ui:573
msgid "°C"
msgstr ""

#: src/extension.js:921
msgid "Calm"
msgstr "Kalm"

#: src/extension.js:924
msgid "Light air"
msgstr "Matige wind"

#: src/extension.js:927
msgid "Light breeze"
msgstr "Zacht briesje"

#: src/extension.js:930
msgid "Gentle breeze"
msgstr "Zacht briesje"

#: src/extension.js:933
msgid "Moderate breeze"
msgstr "Matige wind"

#: src/extension.js:936
msgid "Fresh breeze"
msgstr "Fris briesje"

#: src/extension.js:939
msgid "Strong breeze"
msgstr "Harde wind"

#: src/extension.js:942
msgid "Moderate gale"
msgstr "Matige storm"

#: src/extension.js:945
#, fuzzy
msgid "Fresh gale"
msgstr "Frisse storm"

#: src/extension.js:948
msgid "Strong gale"
msgstr "Harde storm"

#: src/extension.js:951
msgid "Storm"
msgstr "Storm"

#: src/extension.js:954
msgid "Violent storm"
msgstr "Hevige storm"

#: src/extension.js:957
msgid "Hurricane"
msgstr "Orkaan"

#: src/extension.js:961
msgid "Sunday"
msgstr "Zondag"

#: src/extension.js:961
msgid "Monday"
msgstr "Maandag"

#: src/extension.js:961
msgid "Tuesday"
msgstr "Dinsdag"

#: src/extension.js:961
msgid "Wednesday"
msgstr "Woensdag"

#: src/extension.js:961
msgid "Thursday"
msgstr "Donderdag"

#: src/extension.js:961
msgid "Friday"
msgstr "Vrijdag"

#: src/extension.js:961
msgid "Saturday"
msgstr "Zaterdag"

#: src/extension.js:967
msgid "N"
msgstr "N"

#: src/extension.js:967
msgid "NE"
msgstr "NO"

#: src/extension.js:967
msgid "E"
msgstr "O"

#: src/extension.js:967
msgid "SE"
msgstr "SO"

#: src/extension.js:967
msgid "S"
msgstr "Z"

#: src/extension.js:967
msgid "SW"
msgstr "ZW"

#: src/extension.js:967
msgid "W"
msgstr "W"

#: src/extension.js:967
msgid "NW"
msgstr "NW"

#: src/extension.js:1020 src/extension.js:1029 src/weather-settings.ui:607
msgid "hPa"
msgstr ""

#: src/extension.js:1024 src/weather-settings.ui:608
msgid "inHg"
msgstr ""

#: src/extension.js:1034 src/weather-settings.ui:609
msgid "bar"
msgstr ""

#: src/extension.js:1039 src/weather-settings.ui:610
msgid "Pa"
msgstr ""

#: src/extension.js:1044 src/weather-settings.ui:611
msgid "kPa"
msgstr ""

#: src/extension.js:1049 src/weather-settings.ui:612
msgid "atm"
msgstr ""

#: src/extension.js:1054 src/weather-settings.ui:613
msgid "at"
msgstr ""

#: src/extension.js:1059 src/weather-settings.ui:614
msgid "Torr"
msgstr ""

#: src/extension.js:1064 src/weather-settings.ui:615
msgid "psi"
msgstr ""

#: src/extension.js:1069 src/weather-settings.ui:616
msgid "mmHg"
msgstr ""

#: src/extension.js:1074 src/weather-settings.ui:617
msgid "mbar"
msgstr ""

#: src/extension.js:1118 src/weather-settings.ui:593
msgid "m/s"
msgstr ""

#: src/extension.js:1122 src/weather-settings.ui:592
msgid "mph"
msgstr ""

#: src/extension.js:1127 src/weather-settings.ui:591
msgid "km/h"
msgstr ""

#: src/extension.js:1136 src/weather-settings.ui:594
msgid "kn"
msgstr ""

#: src/extension.js:1141 src/weather-settings.ui:595
msgid "ft/s"
msgstr ""

#: src/extension.js:1228
msgid "Loading ..."
msgstr "Laden ..."

#: src/extension.js:1232
msgid "Please wait"
msgstr "Een ogenblik geduld alstublieft"

#: src/extension.js:1303
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1307
msgid "Humidity:"
msgstr "Luchtvochtigheid:"

#: src/extension.js:1311
msgid "Pressure:"
msgstr "Luchtdruk:"

#: src/extension.js:1315
msgid "Wind:"
msgstr "Wind:"

#: src/extension.js:1319
msgid "Gusts:"
msgstr ""

#: src/extension.js:1450
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1452
msgid "Day Forecast"
msgstr ""

#: src/openweathermap_org.js:130
#, fuzzy
msgid "Thunderstorm with light rain"
msgstr "Onweer"

#: src/openweathermap_org.js:132
#, fuzzy
msgid "Thunderstorm with rain"
msgstr "Onweer"

#: src/openweathermap_org.js:134
#, fuzzy
msgid "Thunderstorm with heavy rain"
msgstr "Onweer"

#: src/openweathermap_org.js:136
#, fuzzy
msgid "Light thunderstorm"
msgstr "Geïsoleerde onweer"

#: src/openweathermap_org.js:138
#, fuzzy
msgid "Thunderstorm"
msgstr "Onweer"

#: src/openweathermap_org.js:140
#, fuzzy
msgid "Heavy thunderstorm"
msgstr "Zware onweer"

#: src/openweathermap_org.js:142
#, fuzzy
msgid "Ragged thunderstorm"
msgstr "Geïsoleerde onweer"

#: src/openweathermap_org.js:144
#, fuzzy
msgid "Thunderstorm with light drizzle"
msgstr "Onweer"

#: src/openweathermap_org.js:146
#, fuzzy
msgid "Thunderstorm with drizzle"
msgstr "Onweer"

#: src/openweathermap_org.js:148
#, fuzzy
msgid "Thunderstorm with heavy drizzle"
msgstr "Onweer"

#: src/openweathermap_org.js:150
#, fuzzy
msgid "Light intensity drizzle"
msgstr "Motregen"

#: src/openweathermap_org.js:152
#, fuzzy
msgid "Drizzle"
msgstr "Motregen"

#: src/openweathermap_org.js:154
#, fuzzy
msgid "Heavy intensity drizzle"
msgstr "Regen en hagel"

#: src/openweathermap_org.js:156
#, fuzzy
msgid "Light intensity drizzle rain"
msgstr "Motregen"

#: src/openweathermap_org.js:158
#, fuzzy
msgid "Drizzle rain"
msgstr "Motregen"

#: src/openweathermap_org.js:160
#, fuzzy
msgid "Heavy intensity drizzle rain"
msgstr "Motregen"

#: src/openweathermap_org.js:162
#, fuzzy
msgid "Shower rain and drizzle"
msgstr "Regen en hagel"

#: src/openweathermap_org.js:164
#, fuzzy
msgid "Heavy shower rain and drizzle"
msgstr "Regen en hagel"

#: src/openweathermap_org.js:166
#, fuzzy
msgid "Shower drizzle"
msgstr "Motijzel"

#: src/openweathermap_org.js:168
#, fuzzy
msgid "Light rain"
msgstr "IJzel"

#: src/openweathermap_org.js:170
#, fuzzy
msgid "Moderate rain"
msgstr "Motregen"

#: src/openweathermap_org.js:172
#, fuzzy
msgid "Heavy intensity rain"
msgstr "Hevige regen"

#: src/openweathermap_org.js:174
#, fuzzy
msgid "Very heavy rain"
msgstr "Erg hevige regen"

#: src/openweathermap_org.js:176
#, fuzzy
msgid "Extreme rain"
msgstr "IJzel"

#: src/openweathermap_org.js:178
#, fuzzy
msgid "Freezing rain"
msgstr "IJzel"

#: src/openweathermap_org.js:180
#, fuzzy
msgid "Light intensity shower rain"
msgstr "Lichte sneeuwbuien"

#: src/openweathermap_org.js:182
#, fuzzy
msgid "Shower rain"
msgstr "Regenbuien"

#: src/openweathermap_org.js:184
#, fuzzy
msgid "Heavy intensity shower rain"
msgstr "Zware sneeuwval"

#: src/openweathermap_org.js:186
#, fuzzy
msgid "Ragged shower rain"
msgstr "Hevige regen"

#: src/openweathermap_org.js:188
#, fuzzy
msgid "Light snow"
msgstr "Sneeuwstorm"

#: src/openweathermap_org.js:190
msgid "Snow"
msgstr "sneeuw"

#: src/openweathermap_org.js:192
#, fuzzy
msgid "Heavy snow"
msgstr "Zware sneeuwval"

#: src/openweathermap_org.js:194
#, fuzzy
msgid "Sleet"
msgstr "Stofhagel"

#: src/openweathermap_org.js:196
#, fuzzy
msgid "Shower sleet"
msgstr "Regenbuien"

#: src/openweathermap_org.js:198
#, fuzzy
msgid "Light rain and snow"
msgstr "Regen en sneeuw"

#: src/openweathermap_org.js:200
#, fuzzy
msgid "Rain and snow"
msgstr "Regen en sneeuw"

#: src/openweathermap_org.js:202
#, fuzzy
msgid "Light shower snow"
msgstr "Lichte sneeuwbuien"

#: src/openweathermap_org.js:204
#, fuzzy
msgid "Shower snow"
msgstr "Regenbuien"

#: src/openweathermap_org.js:206
#, fuzzy
msgid "Heavy shower snow"
msgstr "Zware sneeuwval"

#: src/openweathermap_org.js:208
msgid "Mist"
msgstr "Mist"

#: src/openweathermap_org.js:210
msgid "Smoke"
msgstr "rook"

#: src/openweathermap_org.js:212
msgid "Haze"
msgstr "nevel"

#: src/openweathermap_org.js:214
msgid "Sand/Dust Whirls"
msgstr ""

#: src/openweathermap_org.js:216
#, fuzzy
msgid "Fog"
msgstr "Nevelig"

#: src/openweathermap_org.js:218
msgid "Sand"
msgstr "zand"

#: src/openweathermap_org.js:220
msgid "Dust"
msgstr "zand"

#: src/openweathermap_org.js:222
msgid "VOLCANIC ASH"
msgstr "VULKAANAS"

#: src/openweathermap_org.js:224
msgid "SQUALLS"
msgstr "RUKWINDEN"

#: src/openweathermap_org.js:226
msgid "TORNADO"
msgstr "TORNADO"

#: src/openweathermap_org.js:228
msgid "Sky is clear"
msgstr "de lucht is helder"

#: src/openweathermap_org.js:230
msgid "Few clouds"
msgstr "weinig bewolking"

#: src/openweathermap_org.js:232
#, fuzzy
msgid "Scattered clouds"
msgstr "Verspreide buien"

#: src/openweathermap_org.js:234
#, fuzzy
msgid "Broken clouds"
msgstr "Zwaar bewolkt"

#: src/openweathermap_org.js:236
#, fuzzy
msgid "Overcast clouds"
msgstr "Zwaar bewolkt"

#: src/openweathermap_org.js:238
msgid "Not available"
msgstr "Niet beschikbaar"

#: src/openweathermap_org.js:421
msgid "Yesterday"
msgstr "Gisteren"

#: src/openweathermap_org.js:423
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d dag geleden"
msgstr[1] "%d dagen geleden"

#: src/openweathermap_org.js:438 src/openweathermap_org.js:440
msgid ", "
msgstr ""

#: src/openweathermap_org.js:456
msgid "?"
msgstr ""

#: src/openweathermap_org.js:523
msgid "Tomorrow"
msgstr "Morgen"

#: src/prefs.js:189
#, fuzzy
msgid "Searching ..."
msgstr "Laden ..."

#: src/prefs.js:201 src/prefs.js:239 src/prefs.js:270 src/prefs.js:274
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Ongeldige data bij de zoekopdracht voor \"%s\""

#: src/prefs.js:206 src/prefs.js:246 src/prefs.js:277
#, javascript-format
msgid "\"%s\" not found"
msgstr "\"%s\" niet gevonden"

#: src/prefs.js:224
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Persoonlijke AppKey van developer.mapquest.com"

#: src/prefs.js:225
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Persoonlijke AppKey van developer.mapquest.com"

#: src/prefs.js:240
#, fuzzy
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr "Persoonlijke AppKey van developer.mapquest.com"

#: src/prefs.js:241
#, fuzzy
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr "Persoonlijke AppKey van developer.mapquest.com"

#: src/prefs.js:323
msgid "Location"
msgstr "Locatie"

#: src/prefs.js:334
msgid "Provider"
msgstr "Provider"

#: src/prefs.js:344
msgid "Result"
msgstr ""

#: src/prefs.js:548
#, javascript-format
msgid "Remove %s ?"
msgstr "%s verwijderen?"

#: src/prefs.js:561
#, fuzzy
msgid "No"
msgstr "N"

#: src/prefs.js:562
msgid "Yes"
msgstr ""

#: src/prefs.js:1105
msgid "default"
msgstr "standaard"

#: src/weather-settings.ui:29
msgid "Edit name"
msgstr "Naam bewerken"

#: src/weather-settings.ui:40 src/weather-settings.ui:56
#: src/weather-settings.ui:183
#, fuzzy
msgid "Clear entry"
msgstr "Helder"

#: src/weather-settings.ui:47
msgid "Edit coordinates"
msgstr "Coördinaten bewerken"

#: src/weather-settings.ui:80 src/weather-settings.ui:216
msgid "Cancel"
msgstr "Annuleren"

#: src/weather-settings.ui:90 src/weather-settings.ui:226
msgid "Save"
msgstr "Opslaan"

#: src/weather-settings.ui:124
msgid "Search Results"
msgstr ""

#: src/weather-settings.ui:161
#, fuzzy
msgid "Search By Location Or Coordinates"
msgstr "Zoek op locatie of coördinaten"

#: src/weather-settings.ui:184
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "v.b. Vaiaku, Tuvalu of -8.5211767,179.1976747"

#: src/weather-settings.ui:189
msgid "Find"
msgstr "Zoek"

#: src/weather-settings.ui:328
#, fuzzy
msgid "Choose default weather provider"
msgstr "Kies standaard weerprovider"

#: src/weather-settings.ui:339
msgid "Personal Api key from openweathermap.org"
msgstr "Persoonlijke API-key van openweathermap.org"

#: src/weather-settings.ui:371
msgid "Refresh timeout for current weather [min]"
msgstr "Ververs-timeout voor huidig weer [min]"

#: src/weather-settings.ui:383
msgid "Refresh timeout for weather forecast [min]"
msgstr "Ververs-timeout voor vooruitzicht [min]"

#: src/weather-settings.ui:428
msgid "Use extensions api-key for openweathermap.org"
msgstr "Gebruik de API-key van de extensie voor openweathermap.org"

#: src/weather-settings.ui:437
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Zet uit, als u een eigen api-key voor openweathermap.org heeft, en voer deze "
"zo nodig in in het vak hier onder."

#: src/weather-settings.ui:451
msgid "Weather provider"
msgstr "Weerprovider"

#: src/weather-settings.ui:475
#, fuzzy
msgid "Choose geolocation provider"
msgstr "Kies geolocatie provider"

#: src/weather-settings.ui:497
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Persoonlijke AppKey van developer.mapquest.com"

#: src/weather-settings.ui:521
msgid "Geolocation provider"
msgstr "Geolocatie provider"

#: src/weather-settings.ui:545
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr "Temperatuur:"

#: src/weather-settings.ui:554
msgid "Wind Speed Unit"
msgstr "Windsnelheid:"

#: src/weather-settings.ui:563
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
#, fuzzy
msgid "Pressure Unit"
msgstr "Luchtdruk:"

#: src/weather-settings.ui:596
msgid "Beaufort"
msgstr ""

#: src/weather-settings.ui:631
msgid "Units"
msgstr "Eenheden"

#: src/weather-settings.ui:655
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr "Positie in paneel"

#: src/weather-settings.ui:664
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr ""

#: src/weather-settings.ui:673
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr "Windrichting met pijlen"

#: src/weather-settings.ui:682
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
#, fuzzy
msgid "Translate Conditions"
msgstr "Vertaalcondities"

#: src/weather-settings.ui:691
#, fuzzy
msgid "Use System Icon Theme"
msgstr "Symbolische Iconen"

#: src/weather-settings.ui:700
msgid "Text on buttons"
msgstr "Tekst op knoppen"

#: src/weather-settings.ui:709
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr "Temperatuur:"

#: src/weather-settings.ui:718
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr "Toon conditie in paneel"

#: src/weather-settings.ui:727
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/weather-settings.ui:736
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr "Toon conditie bij vooruitzicht"

#: src/weather-settings.ui:745
msgid "Center forecast"
msgstr "Lijn vooruitzicht in het midden uit"

#: src/weather-settings.ui:754
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Number of days in forecast"
msgstr "Aantal dagen in het vooruitzicht"

#: src/weather-settings.ui:763
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Maximal number of digits after the decimal point"
msgstr "Maximaal aantal nummers na de komma"

#: src/weather-settings.ui:777
msgid "Center"
msgstr "Midden"

#: src/weather-settings.ui:778
msgid "Right"
msgstr "Rechts"

#: src/weather-settings.ui:779
msgid "Left"
msgstr "Links"

#: src/weather-settings.ui:926
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: src/weather-settings.ui:950
msgid "Layout"
msgstr "Layout"

#: src/weather-settings.ui:985
msgid "Version: "
msgstr "Versie:"

#: src/weather-settings.ui:999
msgid ""
"<span>Display weather information for any location on Earth in the GNOME "
"Shell</span>"
msgstr ""

#: src/weather-settings.ui:1013
msgid ""
"Weather data provided by: <a href=\"https://openweathermap.org/"
"\">OpenWeatherMap</a>"
msgstr ""

#: src/weather-settings.ui:1026
#, fuzzy
msgid "Maintained by:"
msgstr "Onderhouden door"

#: src/weather-settings.ui:1061
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Dit programma komt met ABSOLUUT GEEN GARANTIE.\n"
"Zie ook de <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, versie 2 of later</a> voor details.</span>"

#: src/weather-settings.ui:1072
msgid "About"
msgstr "Over"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
#, fuzzy
msgid "Weather Provider"
msgstr "Weerprovider"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
#, fuzzy
msgid "Geolocation Provider"
msgstr "Geolocatie provider"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
#, fuzzy
msgid "Wind Speed Units"
msgstr "Windsnelheid:"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
#, fuzzy
msgid "System Icons"
msgstr "Symbolische Iconen"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
#, fuzzy
msgid "Use text on buttons in menu"
msgstr "Tekst op knoppen"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
#, fuzzy
msgid "Refresh interval (forecast)"
msgstr "Lijn vooruitzicht in het midden uit"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
#, fuzzy
msgid "Center forecastbox."
msgstr "Lijn vooruitzicht in het midden uit"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
#, fuzzy
msgid "Your personal API key from openweathermap.org"
msgstr "Persoonlijke API-key van openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
#, fuzzy
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Gebruik de API-key van de extensie voor openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
#, fuzzy
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Persoonlijke AppKey van developer.mapquest.com"

#~ msgid "unknown (self-build ?)"
#~ msgstr "onbekend (zelfbouw?)"

#~ msgid ""
#~ "Dark Sky does not work without an api-key.\n"
#~ "Please register at https://darksky.net/dev/register and paste your "
#~ "personal key into the preferences dialog."
#~ msgstr ""
#~ "Dark Sky werkt niet zonder een api-key.\n"
#~ "Registreer a.u.b. bij https://darksky.net/dev/register en plak de "
#~ "persoonlijke api-key in de voorkeuren."

#~ msgid "Cloudiness:"
#~ msgstr "Bewolking:"

#~ msgid "Today"
#~ msgstr "Vandaag"

#, javascript-format
#~ msgid "In %d day"
#~ msgid_plural "In %d days"
#~ msgstr[0] "Over %d dag"
#~ msgstr[1] "Over %d dagen"

#~ msgid "Extensions default weather provider"
#~ msgstr "Standaard weerprovider voor deze extensie"

#, fuzzy
#~ msgid "Personal Api key from Dark Sky"
#~ msgstr "Persoonlijke API-key van Dark Sky"

#, fuzzy
#~ msgid ""
#~ "Note: the forecast-timout is not used for Dark Sky, because they do not "
#~ "provide seperate downloads for current weather and forecasts."
#~ msgstr ""
#~ "Nb: de vooruitzicht-timeout word niet gebruikt voor Dark Sky, omdat zij "
#~ "het huidige weer en het vooruitzicht niet afsplitsen in aparte downloads."

#, fuzzy
#~ msgid ""
#~ "<span>Weather extension to display weather information from <a href="
#~ "\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
#~ "darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
#~ msgstr ""
#~ "<span>Weer extensie om informatie te tonen van <a href=\"https://"
#~ "openweathermap.org/\">Openweathermap</a> of <a href=\"https://darksky.net"
#~ "\">Dark Sky</a> voor bijna alle locaties in de wereld.</span>"

#~ msgid "Webpage"
#~ msgstr "Webpagina"

#, fuzzy
#~ msgid "Your personal API key from Dark Sky"
#~ msgstr "Persoonlijke API-key van Dark Sky"
