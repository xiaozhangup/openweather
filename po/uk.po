# Ukrainian translation for gnome-shell-extension-openweather
# Copyright (C) 2017
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Vitalii Paslavskyi <vpaslavskyi@gmail.com>, 2017
#
msgid ""
msgstr ""
"Project-Id-Version: 0.4\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-09 14:39-0300\n"
"PO-Revision-Date: 2017-10-31 22:41+0100\n"
"Last-Translator: Vitalii Paslavskyi <vpaslavskyi@gmail.com>, 2017\n"
"Language-Team: Vitalii Paslavskyi <vpaslavskyi@gmail.com>, 2017 \n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: src/extension.js:171
msgid "..."
msgstr "..."

#: src/extension.js:323
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Openweathermap.org не працює без ключа api.\n"
"Увімкніть перемикач для використання стандартного ключа розширень у "
"налаштуванняхвведіть ключ або зареєструйтесь на сторінці https://"
"openweathermap.org/appid та вставте свій персональний ключ у діалоговому "
"вікні налаштувань"

#: src/extension.js:434 src/extension.js:446
#, javascript-format
msgid "Can not connect to %s"
msgstr "Не можу з'єднатись з %s"

#: src/extension.js:737 src/weather-settings.ui:302
msgid "Locations"
msgstr "Місцезнаходження"

#: src/extension.js:741
msgid "Reload Weather Information"
msgstr "Oновити інформацію про погоду"

#: src/extension.js:745
#, fuzzy
msgid "Weather data by:"
msgstr "Дані про погоду надаються:"

#: src/extension.js:748
msgid "Weather Settings"
msgstr "Налаштування погоди"

#: src/extension.js:771
#, javascript-format
msgid "Can not open %s"
msgstr "Не можу відкрити %s"

#: src/extension.js:819 src/prefs.js:1072
msgid "Invalid city"
msgstr "Недійсне місто"

#: src/extension.js:830
msgid "Invalid location! Please try to recreate it."
msgstr "Недійсне місцезнаходження! Будь-ласка, спробуйте створити його знову."

#: src/extension.js:870 src/weather-settings.ui:574
msgid "°F"
msgstr "°F"

#: src/extension.js:872 src/weather-settings.ui:575
msgid "K"
msgstr "К"

#: src/extension.js:874 src/weather-settings.ui:576
msgid "°Ra"
msgstr ""

#: src/extension.js:876 src/weather-settings.ui:577
msgid "°Ré"
msgstr ""

#: src/extension.js:878 src/weather-settings.ui:578
msgid "°Rø"
msgstr ""

#: src/extension.js:880 src/weather-settings.ui:579
msgid "°De"
msgstr ""

#: src/extension.js:882 src/weather-settings.ui:580
msgid "°N"
msgstr ""

#: src/extension.js:884 src/weather-settings.ui:573
msgid "°C"
msgstr "°C"

#: src/extension.js:921
msgid "Calm"
msgstr "Безвітряна погода"

#: src/extension.js:924
msgid "Light air"
msgstr "Тиха погода"

#: src/extension.js:927
msgid "Light breeze"
msgstr "Легкий вітерець"

#: src/extension.js:930
msgid "Gentle breeze"
msgstr "М'який вітер"

#: src/extension.js:933
msgid "Moderate breeze"
msgstr "Помірний вітер"

#: src/extension.js:936
msgid "Fresh breeze"
msgstr "Свіжий бриз"

#: src/extension.js:939
msgid "Strong breeze"
msgstr "Сильний вітер"

#: src/extension.js:942
msgid "Moderate gale"
msgstr "Помірний шторм"

#: src/extension.js:945
msgid "Fresh gale"
msgstr "Середній шторм"

#: src/extension.js:948
msgid "Strong gale"
msgstr "Сильний шторм"

#: src/extension.js:951
msgid "Storm"
msgstr "Шторм"

#: src/extension.js:954
msgid "Violent storm"
msgstr "Потужний шторм"

#: src/extension.js:957
msgid "Hurricane"
msgstr "Ураган"

#: src/extension.js:961
msgid "Sunday"
msgstr "Неділя"

#: src/extension.js:961
msgid "Monday"
msgstr "Понеділок"

#: src/extension.js:961
msgid "Tuesday"
msgstr "Вівторок"

#: src/extension.js:961
msgid "Wednesday"
msgstr "Середа"

#: src/extension.js:961
msgid "Thursday"
msgstr "Четвер"

#: src/extension.js:961
msgid "Friday"
msgstr "П’ятниця"

#: src/extension.js:961
msgid "Saturday"
msgstr "Субота"

#: src/extension.js:967
msgid "N"
msgstr "Пн"

#: src/extension.js:967
msgid "NE"
msgstr "ПнСх"

#: src/extension.js:967
msgid "E"
msgstr "Сх"

#: src/extension.js:967
msgid "SE"
msgstr "ПдСх"

#: src/extension.js:967
msgid "S"
msgstr "Пд"

#: src/extension.js:967
msgid "SW"
msgstr "ПдЗх"

#: src/extension.js:967
msgid "W"
msgstr "Зх"

#: src/extension.js:967
msgid "NW"
msgstr "ПнЗх"

#: src/extension.js:1020 src/extension.js:1029 src/weather-settings.ui:607
msgid "hPa"
msgstr "гПа"

#: src/extension.js:1024 src/weather-settings.ui:608
msgid "inHg"
msgstr "дюйм рт.ст."

#: src/extension.js:1034 src/weather-settings.ui:609
msgid "bar"
msgstr "бар"

#: src/extension.js:1039 src/weather-settings.ui:610
msgid "Pa"
msgstr "Па"

#: src/extension.js:1044 src/weather-settings.ui:611
msgid "kPa"
msgstr "кПа"

#: src/extension.js:1049 src/weather-settings.ui:612
msgid "atm"
msgstr "атм"

#: src/extension.js:1054 src/weather-settings.ui:613
msgid "at"
msgstr "ат"

#: src/extension.js:1059 src/weather-settings.ui:614
msgid "Torr"
msgstr "Тор"

#: src/extension.js:1064 src/weather-settings.ui:615
msgid "psi"
msgstr "фунт/кв.дюйм"

#: src/extension.js:1069 src/weather-settings.ui:616
msgid "mmHg"
msgstr "мм рт.ст."

#: src/extension.js:1074 src/weather-settings.ui:617
msgid "mbar"
msgstr "мбар"

#: src/extension.js:1118 src/weather-settings.ui:593
msgid "m/s"
msgstr "м/с"

#: src/extension.js:1122 src/weather-settings.ui:592
msgid "mph"
msgstr "миль/год"

#: src/extension.js:1127 src/weather-settings.ui:591
msgid "km/h"
msgstr "км/год"

#: src/extension.js:1136 src/weather-settings.ui:594
msgid "kn"
msgstr "вузли"

#: src/extension.js:1141 src/weather-settings.ui:595
msgid "ft/s"
msgstr "фут/с"

#: src/extension.js:1228
msgid "Loading ..."
msgstr "Завантаження ..."

#: src/extension.js:1232
msgid "Please wait"
msgstr "Зачекайте"

#: src/extension.js:1303
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1307
msgid "Humidity:"
msgstr "Вологість:"

#: src/extension.js:1311
msgid "Pressure:"
msgstr "Тиск:"

#: src/extension.js:1315
msgid "Wind:"
msgstr "Вітер:"

#: src/extension.js:1319
msgid "Gusts:"
msgstr ""

#: src/extension.js:1450
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1452
msgid "Day Forecast"
msgstr ""

#: src/openweathermap_org.js:130
#, fuzzy
msgid "Thunderstorm with light rain"
msgstr "Гроза з легким дощем"

#: src/openweathermap_org.js:132
#, fuzzy
msgid "Thunderstorm with rain"
msgstr "Гроза з дощем"

#: src/openweathermap_org.js:134
#, fuzzy
msgid "Thunderstorm with heavy rain"
msgstr "Гроза із сильним дощем"

#: src/openweathermap_org.js:136
#, fuzzy
msgid "Light thunderstorm"
msgstr "Поодинокі грози"

#: src/openweathermap_org.js:138
#, fuzzy
msgid "Thunderstorm"
msgstr "Грози"

#: src/openweathermap_org.js:140
#, fuzzy
msgid "Heavy thunderstorm"
msgstr "Сильні грози"

#: src/openweathermap_org.js:142
#, fuzzy
msgid "Ragged thunderstorm"
msgstr "Переривчасті грози"

#: src/openweathermap_org.js:144
#, fuzzy
msgid "Thunderstorm with light drizzle"
msgstr "Гроза з легкою мрякою"

#: src/openweathermap_org.js:146
#, fuzzy
msgid "Thunderstorm with drizzle"
msgstr "Гроза з мрякою"

#: src/openweathermap_org.js:148
#, fuzzy
msgid "Thunderstorm with heavy drizzle"
msgstr "Гроза із сильною мрякою"

#: src/openweathermap_org.js:150
msgid "Light intensity drizzle"
msgstr "Легка мряка"

#: src/openweathermap_org.js:152
#, fuzzy
msgid "Drizzle"
msgstr "Мряка"

#: src/openweathermap_org.js:154
#, fuzzy
msgid "Heavy intensity drizzle"
msgstr "Сильна мряка"

#: src/openweathermap_org.js:156
#, fuzzy
msgid "Light intensity drizzle rain"
msgstr "Легка мряка"

#: src/openweathermap_org.js:158
#, fuzzy
msgid "Drizzle rain"
msgstr "Дрібний дощ"

#: src/openweathermap_org.js:160
#, fuzzy
msgid "Heavy intensity drizzle rain"
msgstr "Сильна мжичка"

#: src/openweathermap_org.js:162
#, fuzzy
msgid "Shower rain and drizzle"
msgstr "Злива і мряка"

#: src/openweathermap_org.js:164
#, fuzzy
msgid "Heavy shower rain and drizzle"
msgstr "Сильна злива і мжичка"

#: src/openweathermap_org.js:166
#, fuzzy
msgid "Shower drizzle"
msgstr "Паморозь"

#: src/openweathermap_org.js:168
#, fuzzy
msgid "Light rain"
msgstr "Легкий дощ"

#: src/openweathermap_org.js:170
#, fuzzy
msgid "Moderate rain"
msgstr "Переривчастий дощ"

#: src/openweathermap_org.js:172
msgid "Heavy intensity rain"
msgstr "Сильний дощ"

#: src/openweathermap_org.js:174
msgid "Very heavy rain"
msgstr "Дуже сильний дощ"

#: src/openweathermap_org.js:176
#, fuzzy
msgid "Extreme rain"
msgstr "Естремальний дощ"

#: src/openweathermap_org.js:178
#, fuzzy
msgid "Freezing rain"
msgstr "Крижаний дощ"

#: src/openweathermap_org.js:180
#, fuzzy
msgid "Light intensity shower rain"
msgstr "Легка злива"

#: src/openweathermap_org.js:182
#, fuzzy
msgid "Shower rain"
msgstr "Зливи"

#: src/openweathermap_org.js:184
#, fuzzy
msgid "Heavy intensity shower rain"
msgstr "Сильна злива"

#: src/openweathermap_org.js:186
msgid "Ragged shower rain"
msgstr "Переривчасті грози"

#: src/openweathermap_org.js:188
#, fuzzy
msgid "Light snow"
msgstr "Легкий сніг"

#: src/openweathermap_org.js:190
msgid "Snow"
msgstr "Сніг"

#: src/openweathermap_org.js:192
#, fuzzy
msgid "Heavy snow"
msgstr "Сильний снігопад"

#: src/openweathermap_org.js:194
#, fuzzy
msgid "Sleet"
msgstr "Мокрий сніг"

#: src/openweathermap_org.js:196
#, fuzzy
msgid "Shower sleet"
msgstr "Мокрий снігопад"

#: src/openweathermap_org.js:198
#, fuzzy
msgid "Light rain and snow"
msgstr "Легкий дощ та сніг"

#: src/openweathermap_org.js:200
#, fuzzy
msgid "Rain and snow"
msgstr "Дощ та сніг"

#: src/openweathermap_org.js:202
#, fuzzy
msgid "Light shower snow"
msgstr "Невеликий снігопад"

#: src/openweathermap_org.js:204
#, fuzzy
msgid "Shower snow"
msgstr "Снігопад"

#: src/openweathermap_org.js:206
#, fuzzy
msgid "Heavy shower snow"
msgstr "Сильний снігопад"

#: src/openweathermap_org.js:208
msgid "Mist"
msgstr "Туман"

#: src/openweathermap_org.js:210
msgid "Smoke"
msgstr "Димка"

#: src/openweathermap_org.js:212
msgid "Haze"
msgstr "Туман"

#: src/openweathermap_org.js:214
msgid "Sand/Dust Whirls"
msgstr "Піщані/пилові вихрі"

#: src/openweathermap_org.js:216
#, fuzzy
msgid "Fog"
msgstr "Туман"

#: src/openweathermap_org.js:218
msgid "Sand"
msgstr "Пісок"

#: src/openweathermap_org.js:220
msgid "Dust"
msgstr "Пил"

#: src/openweathermap_org.js:222
msgid "VOLCANIC ASH"
msgstr "ВУЛКАНІЧНИЙ ПОПІЛ"

#: src/openweathermap_org.js:224
msgid "SQUALLS"
msgstr "ШКВАЛИ"

#: src/openweathermap_org.js:226
msgid "TORNADO"
msgstr "ТОРНАДО"

#: src/openweathermap_org.js:228
msgid "Sky is clear"
msgstr "Чисте небо"

#: src/openweathermap_org.js:230
#, fuzzy
msgid "Few clouds"
msgstr "Невелика хмарність"

#: src/openweathermap_org.js:232
#, fuzzy
msgid "Scattered clouds"
msgstr "Розкидані хмари"

#: src/openweathermap_org.js:234
#, fuzzy
msgid "Broken clouds"
msgstr "Переважно хмарно"

#: src/openweathermap_org.js:236
#, fuzzy
msgid "Overcast clouds"
msgstr "Хмарно з проясненнями"

#: src/openweathermap_org.js:238
msgid "Not available"
msgstr "Недоступно"

#: src/openweathermap_org.js:421
msgid "Yesterday"
msgstr "Вчора"

#: src/openweathermap_org.js:423
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d день тому"
msgstr[1] "%d дні тому"
msgstr[2] "%d днів тому"

#: src/openweathermap_org.js:438 src/openweathermap_org.js:440
msgid ", "
msgstr ", "

#: src/openweathermap_org.js:456
msgid "?"
msgstr ""

#: src/openweathermap_org.js:523
msgid "Tomorrow"
msgstr "Завтра"

#: src/prefs.js:189
#, fuzzy
msgid "Searching ..."
msgstr "Завантаження ..."

#: src/prefs.js:201 src/prefs.js:239 src/prefs.js:270 src/prefs.js:274
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Недійсні дані під час пошуку \"%s\""

#: src/prefs.js:206 src/prefs.js:246 src/prefs.js:277
#, javascript-format
msgid "\"%s\" not found"
msgstr "\"%s\" не знайдено"

#: src/prefs.js:224
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Ваш особистий ключ з developer.mapquest.com"

#: src/prefs.js:225
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Особистий АРІ ключ від developer.mapquest.com"

#: src/prefs.js:240
#, fuzzy
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr "Ваш особистий ключ з developer.mapquest.com"

#: src/prefs.js:241
#, fuzzy
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr "Особистий АРІ ключ від developer.mapquest.com"

#: src/prefs.js:323
msgid "Location"
msgstr "Місце знаходження"

#: src/prefs.js:334
msgid "Provider"
msgstr "Постачальник"

#: src/prefs.js:344
msgid "Result"
msgstr ""

#: src/prefs.js:548
#, javascript-format
msgid "Remove %s ?"
msgstr "Видалити %s ?"

#: src/prefs.js:561
#, fuzzy
msgid "No"
msgstr "Пн"

#: src/prefs.js:562
msgid "Yes"
msgstr ""

#: src/prefs.js:1105
msgid "default"
msgstr "за замовчуванням"

#: src/weather-settings.ui:29
msgid "Edit name"
msgstr "Редагувати ім'я"

#: src/weather-settings.ui:40 src/weather-settings.ui:56
#: src/weather-settings.ui:183
#, fuzzy
msgid "Clear entry"
msgstr "Очистити запис"

#: src/weather-settings.ui:47
msgid "Edit coordinates"
msgstr "Змінити координати"

#: src/weather-settings.ui:80 src/weather-settings.ui:216
msgid "Cancel"
msgstr "Відміна"

#: src/weather-settings.ui:90 src/weather-settings.ui:226
msgid "Save"
msgstr "Зберегти"

#: src/weather-settings.ui:124
msgid "Search Results"
msgstr ""

#: src/weather-settings.ui:161
#, fuzzy
msgid "Search By Location Or Coordinates"
msgstr "Пошук за місцезнаходженням чи за координатами"

#: src/weather-settings.ui:184
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "наприклад Ваяку, Тувалу чи -8.5211767,179.1976747"

#: src/weather-settings.ui:189
msgid "Find"
msgstr "Знайти"

#: src/weather-settings.ui:328
#, fuzzy
msgid "Choose default weather provider"
msgstr "Обрати постачальника погоди за замовчуванням"

#: src/weather-settings.ui:339
msgid "Personal Api key from openweathermap.org"
msgstr "Персональний ключ Арі з openweathermap.org"

#: src/weather-settings.ui:371
msgid "Refresh timeout for current weather [min]"
msgstr "Оновити тайм-аут для поточної погоди (хв)"

#: src/weather-settings.ui:383
msgid "Refresh timeout for weather forecast [min]"
msgstr "Оновити тайм-аут для прогнозу погоди [хв]"

#: src/weather-settings.ui:428
msgid "Use extensions api-key for openweathermap.org"
msgstr "Використайте Арі ключ додатку для openweathermap.org"

#: src/weather-settings.ui:437
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Вимкніть, якщо ви маєте власний ключ api для openweathermap.org, та вставте "
"його у нижче текстове вікно "

#: src/weather-settings.ui:451
msgid "Weather provider"
msgstr "Постачальник погоди"

#: src/weather-settings.ui:475
#, fuzzy
msgid "Choose geolocation provider"
msgstr "Обрати постачальника геолокації"

#: src/weather-settings.ui:497
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Особистий АРІ ключ від developer.mapquest.com"

#: src/weather-settings.ui:521
msgid "Geolocation provider"
msgstr "Постачальник геолокації"

#: src/weather-settings.ui:545
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr "Одиниця температури:"

#: src/weather-settings.ui:554
msgid "Wind Speed Unit"
msgstr "Одиниця швидкості вітру"

#: src/weather-settings.ui:563
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
#, fuzzy
msgid "Pressure Unit"
msgstr "Одиниця тиску:"

#: src/weather-settings.ui:596
msgid "Beaufort"
msgstr "Шкала Бофорта"

#: src/weather-settings.ui:631
msgid "Units"
msgstr "Одиниці"

#: src/weather-settings.ui:655
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr "Розміщення на панелі"

#: src/weather-settings.ui:664
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Розташування меню [%] з 0 (ліворуч) до 100 (праворуч)"

#: src/weather-settings.ui:673
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr "Напрямок вітру за стрілками"

#: src/weather-settings.ui:682
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr "Умови перекладу"

#: src/weather-settings.ui:691
#, fuzzy
msgid "Use System Icon Theme"
msgstr "Символьні піктограми"

#: src/weather-settings.ui:700
msgid "Text on buttons"
msgstr "Текст на кнопках"

#: src/weather-settings.ui:709
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr "Температура на панелі"

#: src/weather-settings.ui:718
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr "Погодні умови на панелі"

#: src/weather-settings.ui:727
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/weather-settings.ui:736
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr "Погодні умови у прогнозі"

#: src/weather-settings.ui:745
msgid "Center forecast"
msgstr "Центрувати прогноз"

#: src/weather-settings.ui:754
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Number of days in forecast"
msgstr "Кількість днів у прогнозі"

#: src/weather-settings.ui:763
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Maximal number of digits after the decimal point"
msgstr "Найбільша кількість цифр після коми"

#: src/weather-settings.ui:777
msgid "Center"
msgstr "Центрувати"

#: src/weather-settings.ui:778
msgid "Right"
msgstr "Проворуч"

#: src/weather-settings.ui:779
msgid "Left"
msgstr "Ліворуч"

#: src/weather-settings.ui:926
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: src/weather-settings.ui:950
msgid "Layout"
msgstr "Макет"

#: src/weather-settings.ui:985
msgid "Version: "
msgstr "Версія:"

#: src/weather-settings.ui:999
msgid ""
"<span>Display weather information for any location on Earth in the GNOME "
"Shell</span>"
msgstr ""

#: src/weather-settings.ui:1013
msgid ""
"Weather data provided by: <a href=\"https://openweathermap.org/"
"\">OpenWeatherMap</a>"
msgstr ""

#: src/weather-settings.ui:1026
#, fuzzy
msgid "Maintained by:"
msgstr "Підтримка"

#: src/weather-settings.ui:1061
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Дана програма розповсюджується АБСОЛЮТНО БЕЗ ГАРАНТІЇ.\n"
"Ознайомтесь додатково <a href=\"https://www.gnu.org/licenses/old-licenses/"
"gpl-2.0.html\">GNU General Public License, версія 2 чи пізніша r</a> для "
"додаткових деталей.</span>"

#: src/weather-settings.ui:1072
msgid "About"
msgstr "Про програму"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
msgid "Weather Provider"
msgstr "Постачальник погоди"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
msgid "Geolocation Provider"
msgstr "Постачальник геолокації"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
msgid "Wind Speed Units"
msgstr "Одиниці швидкості вітру"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Оберіть одиниці виміру для швидкості вітру. Дозволені значення 'км/год', "
"'миль/год', 'м/сс', 'вузли', 'ft/s' чи 'Шкала Бофорда'."

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Оберіть як відображати напрямок вітру, срілками чи буквами"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr "Місто для відображення"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr "Поточне місто"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
#, fuzzy
msgid "System Icons"
msgstr "Символьні піктограми"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "Use text on buttons in menu"
msgstr "Використовувати текстові кнопки у меню"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr "Горизонтальне розміщення меню"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr "Інтервал оновлень (поточна погода)"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
msgid "Refresh interval (forecast)"
msgstr "Оновити інтервал (пргноз)"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
msgid "Center forecastbox."
msgstr "Центрувати вікно прогнозу"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
msgid "Your personal API key from openweathermap.org"
msgstr "Ваш особистий АЗІ ключ з openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Використати АРІ ключ за замовчуванням з openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Ваш особистий ключ з developer.mapquest.com"

#~ msgid "unknown (self-build ?)"
#~ msgstr "невідмо"

#~ msgid ""
#~ "Dark Sky does not work without an api-key.\n"
#~ "Please register at https://darksky.net/dev/register and paste your "
#~ "personal key into the preferences dialog."
#~ msgstr ""
#~ "Dark Sky не працює без відповідного ключа api.\n"
#~ "Будь-ласка, зареєструйтесь на сторінці https://darksky.net/dev/register і "
#~ "вставте ваш особистий ключ у діалогове вікно налаштувань."

#~ msgid "Cloudiness:"
#~ msgstr "Хмарність:"

#~ msgid "Today"
#~ msgstr "Сьогодні"

#, javascript-format
#~ msgid "In %d day"
#~ msgid_plural "In %d days"
#~ msgstr[0] "Через %d день"
#~ msgstr[1] "Через %d дні"
#~ msgstr[2] "Через %d днів"

#~ msgid "Extensions default weather provider"
#~ msgstr "Постачальник погоди за замовчуванням"

#~ msgid "Personal Api key from Dark Sky"
#~ msgstr "Особистий Api ключ з Dark Sky"

#~ msgid ""
#~ "Note: the forecast-timout is not used for Dark Sky, because they do not "
#~ "provide seperate downloads for current weather and forecasts."
#~ msgstr ""
#~ "Примітка: тайм-аут прогнозу погоди не застусовується для Dark Sky, "
#~ "оскільки вони не надають окремих завантажень для поточної погодити та "
#~ "прогнозу."

#~ msgid ""
#~ "<span>Weather extension to display weather information from <a href="
#~ "\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
#~ "darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
#~ msgstr ""
#~ "<span>Погодній додаток для відображення інформації про погоду з <a href="
#~ "\"https://openweathermap.org/\">Openweathermap</a> або <a href=\"https://"
#~ "darksky.net\">Dark Sky</a> для більшості міст на світі.</span>"

#~ msgid "Webpage"
#~ msgstr "Сторінка в інтернеті"

#~ msgid "Your personal API key from Dark Sky"
#~ msgstr "Ваш особистий АРІ ключ з Dark Sky"
