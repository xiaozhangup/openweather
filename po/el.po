# greek translation for gnome-shell-extension-openweather
# Copyright (C) 2011
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
#
msgid ""
msgstr ""
"Project-Id-Version: 3.0.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-09 14:39-0300\n"
"PO-Revision-Date: 2014-05-06 20:14+0200\n"
"Last-Translator: poseidaon <poseidaon@users.noreply.github.com>\n"
"Language-Team: \n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Poedit 1.5.4\n"

#: src/extension.js:171
msgid "..."
msgstr "..."

#: src/extension.js:323
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""

#: src/extension.js:434 src/extension.js:446
#, fuzzy, javascript-format
msgid "Can not connect to %s"
msgstr "Αδύνατο το άνοιγμα %s"

#: src/extension.js:737 src/weather-settings.ui:302
msgid "Locations"
msgstr "Τοποθεσίες"

#: src/extension.js:741
msgid "Reload Weather Information"
msgstr "Επαναφόρτωση Πληροφοριών Καιρού"

#: src/extension.js:745
#, fuzzy
msgid "Weather data by:"
msgstr "Πάροχος πληροφοριών καιρού:"

#: src/extension.js:748
msgid "Weather Settings"
msgstr "Ρυθμίσεις Καιρού"

#: src/extension.js:771
#, javascript-format
msgid "Can not open %s"
msgstr "Αδύνατο το άνοιγμα %s"

#: src/extension.js:819 src/prefs.js:1072
msgid "Invalid city"
msgstr "Μη έγκυρη πόλη"

#: src/extension.js:830
msgid "Invalid location! Please try to recreate it."
msgstr ""

#: src/extension.js:870 src/weather-settings.ui:574
msgid "°F"
msgstr ""

#: src/extension.js:872 src/weather-settings.ui:575
msgid "K"
msgstr ""

#: src/extension.js:874 src/weather-settings.ui:576
msgid "°Ra"
msgstr ""

#: src/extension.js:876 src/weather-settings.ui:577
msgid "°Ré"
msgstr ""

#: src/extension.js:878 src/weather-settings.ui:578
msgid "°Rø"
msgstr ""

#: src/extension.js:880 src/weather-settings.ui:579
msgid "°De"
msgstr ""

#: src/extension.js:882 src/weather-settings.ui:580
msgid "°N"
msgstr ""

#: src/extension.js:884 src/weather-settings.ui:573
msgid "°C"
msgstr ""

#: src/extension.js:921
msgid "Calm"
msgstr "Ήρεμος"

#: src/extension.js:924
msgid "Light air"
msgstr "Ελαφριά αύρα"

#: src/extension.js:927
msgid "Light breeze"
msgstr "Ελαφρύ αεράκι"

#: src/extension.js:930
msgid "Gentle breeze"
msgstr "Ήρεμο αεράκι"

#: src/extension.js:933
msgid "Moderate breeze"
msgstr "Μέτριο αεράκι"

#: src/extension.js:936
msgid "Fresh breeze"
msgstr "Δροσερό αεράκι"

#: src/extension.js:939
msgid "Strong breeze"
msgstr "Δυνατό αεράκι"

#: src/extension.js:942
msgid "Moderate gale"
msgstr "Μέτρια ανεμοθύελλα"

#: src/extension.js:945
msgid "Fresh gale"
msgstr "Δροσερή ανεμοθύελλα"

#: src/extension.js:948
msgid "Strong gale"
msgstr "Δυνατή ανεμοθύελα"

#: src/extension.js:951
msgid "Storm"
msgstr "Θύελλα"

#: src/extension.js:954
msgid "Violent storm"
msgstr "Βίαιη θύελλα"

#: src/extension.js:957
msgid "Hurricane"
msgstr "Τυφώνας"

#: src/extension.js:961
msgid "Sunday"
msgstr "Κυριακή"

#: src/extension.js:961
msgid "Monday"
msgstr "Δευτέρα"

#: src/extension.js:961
msgid "Tuesday"
msgstr "Τρίτη"

#: src/extension.js:961
msgid "Wednesday"
msgstr "Τετάρτη"

#: src/extension.js:961
msgid "Thursday"
msgstr "Πέμπτη"

#: src/extension.js:961
msgid "Friday"
msgstr "Παρασκευή"

#: src/extension.js:961
msgid "Saturday"
msgstr "Σάββατο"

#: src/extension.js:967
msgid "N"
msgstr "Β"

#: src/extension.js:967
msgid "NE"
msgstr "ΒΑ"

#: src/extension.js:967
msgid "E"
msgstr "Α"

#: src/extension.js:967
msgid "SE"
msgstr "ΝΑ"

#: src/extension.js:967
msgid "S"
msgstr "Ν"

#: src/extension.js:967
msgid "SW"
msgstr "ΝΔ"

#: src/extension.js:967
msgid "W"
msgstr "Δ"

#: src/extension.js:967
msgid "NW"
msgstr "ΒΔ"

#: src/extension.js:1020 src/extension.js:1029 src/weather-settings.ui:607
msgid "hPa"
msgstr ""

#: src/extension.js:1024 src/weather-settings.ui:608
msgid "inHg"
msgstr ""

#: src/extension.js:1034 src/weather-settings.ui:609
msgid "bar"
msgstr ""

#: src/extension.js:1039 src/weather-settings.ui:610
msgid "Pa"
msgstr ""

#: src/extension.js:1044 src/weather-settings.ui:611
msgid "kPa"
msgstr ""

#: src/extension.js:1049 src/weather-settings.ui:612
msgid "atm"
msgstr ""

#: src/extension.js:1054 src/weather-settings.ui:613
msgid "at"
msgstr ""

#: src/extension.js:1059 src/weather-settings.ui:614
msgid "Torr"
msgstr ""

#: src/extension.js:1064 src/weather-settings.ui:615
msgid "psi"
msgstr ""

#: src/extension.js:1069 src/weather-settings.ui:616
msgid "mmHg"
msgstr ""

#: src/extension.js:1074 src/weather-settings.ui:617
msgid "mbar"
msgstr ""

#: src/extension.js:1118 src/weather-settings.ui:593
msgid "m/s"
msgstr ""

#: src/extension.js:1122 src/weather-settings.ui:592
msgid "mph"
msgstr ""

#: src/extension.js:1127 src/weather-settings.ui:591
msgid "km/h"
msgstr ""

#: src/extension.js:1136 src/weather-settings.ui:594
msgid "kn"
msgstr ""

#: src/extension.js:1141 src/weather-settings.ui:595
msgid "ft/s"
msgstr ""

#: src/extension.js:1228
msgid "Loading ..."
msgstr "Φόρτωση..."

#: src/extension.js:1232
msgid "Please wait"
msgstr "Παρακαλώ περιμένετε"

#: src/extension.js:1303
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1307
msgid "Humidity:"
msgstr "Υγρασία:"

#: src/extension.js:1311
msgid "Pressure:"
msgstr "Πίεση:"

#: src/extension.js:1315
msgid "Wind:"
msgstr "Άνεμος:"

#: src/extension.js:1319
msgid "Gusts:"
msgstr ""

#: src/extension.js:1450
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1452
msgid "Day Forecast"
msgstr ""

#: src/openweathermap_org.js:130
msgid "Thunderstorm with light rain"
msgstr "καταιγίδα με ελαφριά βροχή"

#: src/openweathermap_org.js:132
msgid "Thunderstorm with rain"
msgstr "καταιγίδα με βροχή"

#: src/openweathermap_org.js:134
msgid "Thunderstorm with heavy rain"
msgstr "καταιγίδα με έντονη βροχόπτωση"

#: src/openweathermap_org.js:136
msgid "Light thunderstorm"
msgstr "ελαφριά καταιγίδα"

#: src/openweathermap_org.js:138
msgid "Thunderstorm"
msgstr "καταιγίδα"

#: src/openweathermap_org.js:140
msgid "Heavy thunderstorm"
msgstr "έντονη καταιγίδα"

#: src/openweathermap_org.js:142
msgid "Ragged thunderstorm"
msgstr "ισχυρή καταιγίδα"

#: src/openweathermap_org.js:144
msgid "Thunderstorm with light drizzle"
msgstr "καταιγίδα με ελαφρύ ψιχάλισμα"

#: src/openweathermap_org.js:146
msgid "Thunderstorm with drizzle"
msgstr "καταιγίδα με ψιχάλισμα"

#: src/openweathermap_org.js:148
msgid "Thunderstorm with heavy drizzle"
msgstr "καταιγίδα με έντονο ψιχάλισμα"

#: src/openweathermap_org.js:150
msgid "Light intensity drizzle"
msgstr "ελαφράς έντασης ψιχάλισμα"

#: src/openweathermap_org.js:152
msgid "Drizzle"
msgstr "ψιχάλες"

#: src/openweathermap_org.js:154
msgid "Heavy intensity drizzle"
msgstr "μεγάλης έντασης ψιχάλισμα"

#: src/openweathermap_org.js:156
msgid "Light intensity drizzle rain"
msgstr "ελαφράς έντασης ψιλόβροχο"

#: src/openweathermap_org.js:158
msgid "Drizzle rain"
msgstr "ψιλόβροχο"

#: src/openweathermap_org.js:160
msgid "Heavy intensity drizzle rain"
msgstr "μεγάλης έντασης ψιλόβροχο"

#: src/openweathermap_org.js:162
msgid "Shower rain and drizzle"
msgstr "μπόρα με βροχή και ψιλόβροχο"

#: src/openweathermap_org.js:164
msgid "Heavy shower rain and drizzle"
msgstr "έντονη μπόρα με βροχή και ψιλόβροχο"

#: src/openweathermap_org.js:166
msgid "Shower drizzle"
msgstr "μπόρα με ψιλόβροχο"

#: src/openweathermap_org.js:168
msgid "Light rain"
msgstr "ελαφριά βροχή"

#: src/openweathermap_org.js:170
msgid "Moderate rain"
msgstr "μέτρια βροχή"

#: src/openweathermap_org.js:172
msgid "Heavy intensity rain"
msgstr "μεγάλης έντασης βροχή"

#: src/openweathermap_org.js:174
msgid "Very heavy rain"
msgstr "έντονη βροχόπτωση"

#: src/openweathermap_org.js:176
msgid "Extreme rain"
msgstr "ακραία βροχόπτωση"

#: src/openweathermap_org.js:178
msgid "Freezing rain"
msgstr "παγωμένη βροχή"

#: src/openweathermap_org.js:180
msgid "Light intensity shower rain"
msgstr "ελαφράς έντασης μπόρα με βροχή"

#: src/openweathermap_org.js:182
msgid "Shower rain"
msgstr "μπόρα με βροχή"

#: src/openweathermap_org.js:184
msgid "Heavy intensity shower rain"
msgstr "μεγάλης έντασης μπόρα με βροχή"

#: src/openweathermap_org.js:186
msgid "Ragged shower rain"
msgstr "ισχυρή μπόρα με βροχή"

#: src/openweathermap_org.js:188
msgid "Light snow"
msgstr "ελαφριά χιονόπτωση"

#: src/openweathermap_org.js:190
msgid "Snow"
msgstr "χιονόπτωση"

#: src/openweathermap_org.js:192
msgid "Heavy snow"
msgstr "έντονη χιονόπτωση"

#: src/openweathermap_org.js:194
msgid "Sleet"
msgstr "πάγος"

#: src/openweathermap_org.js:196
msgid "Shower sleet"
msgstr "παγωμένο χιόνι"

#: src/openweathermap_org.js:198
msgid "Light rain and snow"
msgstr "ελαφριά βροχή και χιόνι"

#: src/openweathermap_org.js:200
msgid "Rain and snow"
msgstr "βροχή και χιόνι"

#: src/openweathermap_org.js:202
msgid "Light shower snow"
msgstr "ελαφριά χιονοθύελλα"

#: src/openweathermap_org.js:204
msgid "Shower snow"
msgstr "χιονοθύελλα"

#: src/openweathermap_org.js:206
msgid "Heavy shower snow"
msgstr "έντονη χιονοθύελλα"

#: src/openweathermap_org.js:208
msgid "Mist"
msgstr "ομίχλη"

#: src/openweathermap_org.js:210
msgid "Smoke"
msgstr "υδρατμοί"

#: src/openweathermap_org.js:212
msgid "Haze"
msgstr "καταχνιά"

#: src/openweathermap_org.js:214
msgid "Sand/Dust Whirls"
msgstr "Στρόβιλοι Άμμου/Σκόνης"

#: src/openweathermap_org.js:216
msgid "Fog"
msgstr "Ομιχλώδης"

#: src/openweathermap_org.js:218
msgid "Sand"
msgstr "άμμος"

#: src/openweathermap_org.js:220
msgid "Dust"
msgstr "σκόνη"

#: src/openweathermap_org.js:222
msgid "VOLCANIC ASH"
msgstr "ΗΦΑΙΣΤΙΑΚΗ ΤΕΦΡΑ"

#: src/openweathermap_org.js:224
msgid "SQUALLS"
msgstr "ΜΠΟΥΡΙΝΙ"

#: src/openweathermap_org.js:226
msgid "TORNADO"
msgstr "ΑΝΕΜΟΣΤΡΟΒΙΛΟΣ"

#: src/openweathermap_org.js:228
msgid "Sky is clear"
msgstr "καθαρός ουρανός"

#: src/openweathermap_org.js:230
msgid "Few clouds"
msgstr "λίγα σύννεφα"

#: src/openweathermap_org.js:232
msgid "Scattered clouds"
msgstr "σποραδικές νεφώσεις"

#: src/openweathermap_org.js:234
msgid "Broken clouds"
msgstr "διάσπαρτα σύννεφα"

#: src/openweathermap_org.js:236
msgid "Overcast clouds"
msgstr "κυρίως νεφελώδης"

#: src/openweathermap_org.js:238
msgid "Not available"
msgstr "Μη διαθέσιμο"

#: src/openweathermap_org.js:421
msgid "Yesterday"
msgstr "Εχθές"

#: src/openweathermap_org.js:423
#, fuzzy, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "εδώ και %s μέρες"
msgstr[1] "εδώ και %s μέρες"

#: src/openweathermap_org.js:438 src/openweathermap_org.js:440
msgid ", "
msgstr ""

#: src/openweathermap_org.js:456
msgid "?"
msgstr ""

#: src/openweathermap_org.js:523
msgid "Tomorrow"
msgstr "Αύριο"

#: src/prefs.js:189
#, fuzzy
msgid "Searching ..."
msgstr "Φόρτωση..."

#: src/prefs.js:201 src/prefs.js:239 src/prefs.js:270 src/prefs.js:274
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr ""

#: src/prefs.js:206 src/prefs.js:246 src/prefs.js:277
#, fuzzy, javascript-format
msgid "\"%s\" not found"
msgstr "Το Σχήμα \"%s\" δε βρέθηκε"

#: src/prefs.js:224
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/prefs.js:225
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/prefs.js:240
#, fuzzy
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/prefs.js:241
#, fuzzy
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/prefs.js:323
#, fuzzy
msgid "Location"
msgstr "Τοποθεσίες"

#: src/prefs.js:334
#, fuzzy
msgid "Provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: src/prefs.js:344
msgid "Result"
msgstr ""

#: src/prefs.js:548
#, javascript-format
msgid "Remove %s ?"
msgstr "Αφαίρεση %s ;"

#: src/prefs.js:561
#, fuzzy
msgid "No"
msgstr "Β"

#: src/prefs.js:562
msgid "Yes"
msgstr ""

#: src/prefs.js:1105
msgid "default"
msgstr ""

#: src/weather-settings.ui:29
msgid "Edit name"
msgstr ""

#: src/weather-settings.ui:40 src/weather-settings.ui:56
#: src/weather-settings.ui:183
#, fuzzy
msgid "Clear entry"
msgstr "Καθαρός"

#: src/weather-settings.ui:47
msgid "Edit coordinates"
msgstr ""

#: src/weather-settings.ui:80 src/weather-settings.ui:216
msgid "Cancel"
msgstr ""

#: src/weather-settings.ui:90 src/weather-settings.ui:226
msgid "Save"
msgstr ""

#: src/weather-settings.ui:124
msgid "Search Results"
msgstr ""

#: src/weather-settings.ui:161
msgid "Search By Location Or Coordinates"
msgstr ""

#: src/weather-settings.ui:184
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr ""

#: src/weather-settings.ui:189
msgid "Find"
msgstr ""

#: src/weather-settings.ui:328
#, fuzzy
msgid "Choose default weather provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: src/weather-settings.ui:339
msgid "Personal Api key from openweathermap.org"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/weather-settings.ui:371
msgid "Refresh timeout for current weather [min]"
msgstr ""

#: src/weather-settings.ui:383
#, fuzzy
msgid "Refresh timeout for weather forecast [min]"
msgstr "Πρόγνωση στο κέντρο"

#: src/weather-settings.ui:428
#, fuzzy
msgid "Use extensions api-key for openweathermap.org"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/weather-settings.ui:437
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""

#: src/weather-settings.ui:451
#, fuzzy
msgid "Weather provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: src/weather-settings.ui:475
#, fuzzy
msgid "Choose geolocation provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: src/weather-settings.ui:497
#, fuzzy
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: src/weather-settings.ui:521
#, fuzzy
msgid "Geolocation provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: src/weather-settings.ui:545
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
msgid "Temperature Unit"
msgstr "Μονάδα Θερμοκρασίας"

#: src/weather-settings.ui:554
msgid "Wind Speed Unit"
msgstr "Μονάδα Ταχύτητας Ανέμου"

#: src/weather-settings.ui:563
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
msgid "Pressure Unit"
msgstr "Μονάδα Πίεσης"

#: src/weather-settings.ui:596
msgid "Beaufort"
msgstr ""

#: src/weather-settings.ui:631
msgid "Units"
msgstr ""

#: src/weather-settings.ui:655
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr "Θέση στον Πίνακα"

#: src/weather-settings.ui:664
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr ""

#: src/weather-settings.ui:673
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr "Κατεύθυνση Ανέμου με Βέλη"

#: src/weather-settings.ui:682
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr "Μετάφραση Καιρικών Συνθηκών"

#: src/weather-settings.ui:691
#, fuzzy
msgid "Use System Icon Theme"
msgstr "Εικονίδια Συμβόλων"

#: src/weather-settings.ui:700
msgid "Text on buttons"
msgstr ""

#: src/weather-settings.ui:709
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
msgid "Temperature in Panel"
msgstr "Θερμοκρασία στον Πίνακα"

#: src/weather-settings.ui:718
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr "Συνθήκες στον πίνακα"

#: src/weather-settings.ui:727
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/weather-settings.ui:736
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
#, fuzzy
msgid "Conditions in Forecast"
msgstr "Συνθήκες στον πίνακα"

#: src/weather-settings.ui:745
msgid "Center forecast"
msgstr "Πρόγνωση στο κέντρο"

#: src/weather-settings.ui:754
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Number of days in forecast"
msgstr "Αριθμός ημερών πρόγνωσης"

#: src/weather-settings.ui:763
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Maximal number of digits after the decimal point"
msgstr "Μέγιστος αριθμός ψηφίων μετά την υποδιαστολή"

#: src/weather-settings.ui:777
msgid "Center"
msgstr "Κέντρο"

#: src/weather-settings.ui:778
msgid "Right"
msgstr "Δεξιά"

#: src/weather-settings.ui:779
msgid "Left"
msgstr "Αριστερά"

#: src/weather-settings.ui:926
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: src/weather-settings.ui:950
msgid "Layout"
msgstr ""

#: src/weather-settings.ui:985
msgid "Version: "
msgstr ""

#: src/weather-settings.ui:999
msgid ""
"<span>Display weather information for any location on Earth in the GNOME "
"Shell</span>"
msgstr ""

#: src/weather-settings.ui:1013
msgid ""
"Weather data provided by: <a href=\"https://openweathermap.org/"
"\">OpenWeatherMap</a>"
msgstr ""

#: src/weather-settings.ui:1026
msgid "Maintained by:"
msgstr ""

#: src/weather-settings.ui:1061
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""

#: src/weather-settings.ui:1072
msgid "About"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
#, fuzzy
msgid "Weather Provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
#, fuzzy
msgid "Geolocation Provider"
msgstr "Πάροχος πληροφοριών καιρού:"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
#, fuzzy
msgid "Wind Speed Units"
msgstr "Μονάδα Ταχύτητας Ανέμου"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
#, fuzzy
msgid "System Icons"
msgstr "Εικονίδια Συμβόλων"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "Use text on buttons in menu"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
#, fuzzy
msgid "Refresh interval (actual weather)"
msgstr "Πρόγνωση στο κέντρο"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
#, fuzzy
msgid "Refresh interval (forecast)"
msgstr "Πρόγνωση στο κέντρο"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
#, fuzzy
msgid "Center forecastbox."
msgstr "Πρόγνωση στο κέντρο"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
#, fuzzy
msgid "Your personal API key from openweathermap.org"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
#, fuzzy
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
#, fuzzy
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#~ msgid "Cloudiness:"
#~ msgstr "Συννεφιά:"

#~ msgid "Today"
#~ msgstr "Σήμερα"

#, fuzzy, javascript-format
#~ msgid "In %d day"
#~ msgid_plural "In %d days"
#~ msgstr[0] "Σε %s μέρες"
#~ msgstr[1] "Σε %s μέρες"

#, fuzzy
#~ msgid "Personal Api key from Dark Sky"
#~ msgstr "Προσωπικό κλειδί Api από το openweathermap.org"

#, fuzzy
#~ msgid "Your personal API key from Dark Sky"
#~ msgstr "Προσωπικό κλειδί Api από το openweathermap.org"
